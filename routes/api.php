<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'product'], function()
{
    Route::get('', ['as' => 'products.index', 'uses' => 'ProductController@index']);
    Route::delete('delete', ['as' => 'products.delete', 'uses' => 'ProductController@delete']);
    Route::post('upload', ['as' => 'products.upload', 'uses' => 'ProductController@upload']);
});
