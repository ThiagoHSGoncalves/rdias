<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use RDias\Repositories\ProductRepository;
use Illuminate\Contracts\Routing\ResponseFactory;
use App\RDias\Services\Validators\ProductValidator as Validator;
use App\Jobs\ImportProducts;

class ProductController extends Controller
{
	protected $repo;
	protected $response;
	protected $validator;

	public function __construct(ProductRepository $productRepository, ResponseFactory $response, Validator $validator)
	{
		$this->repo = $productRepository;
		$this->response = $response;
		$this->validator = $validator;
	}

	public function index()
	{
		$products = $this->repo->all();
		return $this->response->json($products);
		#return $this->response->view('products.index', compact('products'));
	}

	public function list()
	{
		$products = $this->repo->all();
		return $this->response->view('products.index', compact('products'));
	}

	public function upload()
	{
		$inputFile = \Request::file('file');

		if (is_null($inputFile)) {
			abort(400, 'nenhum arquivo recebido.');
		}

        $ext = $inputFile->clientExtension();

		if ($ext !== 'xlsx') {
			abort(400, 'extensao invalida');
		}

		$fileName = (date('Ymd-His')).'.'.$ext;

		$inputFile->move(storage_path('app/import'), $fileName);

        $this->dispatch(new ImportProducts());

		return response()->json('arquivo aguardando processamento', 200);
	}

	public function delete(Request $request)
	{
	    $id = $request->get('id');
		$response = $this->repo->delete($id);

		if (is_null($response)) {
			response()->json('invalid product id', 400);
		}

		response()->json('success', 200);
	}
}
