<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Product;
use Exception;
use File as file;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ImportProducts extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $path;
    private $inProgressPath;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->path = storage_path('app/import/');
        $this->inProgressPath = storage_path('app/import_inprogress/');
    }

    /**
    * List files from path
    */
    public function getFiles($path)
    {
        if (!isset($path) || !is_dir($path)) {
            return false;
        }

        $files = file::allFiles($path);

        if (empty($files)) {
            return null;
        }

        return $files;
    }

    public function loadWorksheet($filePath)
    {
        if (!file_exists($filePath)) {
            return false;
        }

        $reader = IOFactory::createReader('Xlsx');
        $reader->setReadDataOnly(TRUE);
        $spreadsheet = $reader->load($filePath);
        $worksheet = $spreadsheet->getActiveSheet()->toArray();
        unset($worksheet[0]); // skip header

        $ret = [];
        foreach ($worksheet as $line => $column) {
            array_push($ret, [
                'id' => (int)$column[0],
                'category' => strtolower(filter_var($column[5], FILTER_SANITIZE_STRING)),
                'name' => filter_var($column[1], FILTER_SANITIZE_STRING),
                'free_shipping' => (int)$column[2],
                'description' => filter_var($column[3], FILTER_SANITIZE_STRING),
                'price' => filter_var(
                    $column[4],
                    FILTER_SANITIZE_NUMBER_FLOAT,
                    FILTER_FLAG_ALLOW_FRACTION
                )
            ]);
        }
        return $ret;
    }

    /**
     * Execute the job.
     * @return void
     */
    public function handle()
    {
        $this->createInProgressDirectory();
        \DB::beginTransaction();
        try {
            $filesList = $this->getFiles($this->path);

            if ($filesList === null) {
                return;
            }

            if (!is_array($filesList)) {
                throw new Exception("Erro ao listar arquivos.");
            }

            $name = '';
            // itero todos os arquivos da pasta
            foreach ($filesList as $excel) {
                $name = $excel->getRelativePathName();
                file::move($this->path.$name, $this->inProgressPath.$name);
                $worksheet  = $this->loadWorksheet($this->inProgressPath.$name);
                if (is_null($worksheet)) {
                    continue;
                }

                foreach ($worksheet as $idx => $line) {
                    if (!Product::updateOrCreate(['id'=>$line['id']], $line)) {
                        throw new Exception("Erro ao executar update/insert na base.");
                    }
                }
            }

            \DB::commit();
            file::delete($this->inProgressPath.$name);
            return true;
        } catch (Exception $e) {
            \DB::rollback();
            return false;
        }
    }

    public function createInProgressDirectory()
    {
        if (!is_dir($this->inProgressPath)) {
            Storage::makeDirectory('import_inprogress');
        }
        return;
    }
}
