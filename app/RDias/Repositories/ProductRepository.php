<?php

namespace RDias\Repositories;

use App;

class ProductRepository implements IRepository
{
	public function all($columns = ['*'])
	{
		return $this->getEntity()->get($columns);
	}

	public function find($id, $columns = ['*'])
	{
		return $this->getEntity()->find($id, $columns);
	}

	public function create(array $data)
	{
		return $this->getEntity()->create($data);
	}

	public function update(array $data, $id)
	{
		unset($data['_token']);
		return $this->getEntity()->where('lm', '=', $id)->update($data);
	}

	public function delete($id)
	{
		return $this->getEntity()->destroy($id);
	}

	protected function getEntity()
	{
		return App::make('App\Product');
	}
}
