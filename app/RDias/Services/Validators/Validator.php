<?php

namespace App\RDias\Services\Validators;

use \Illuminate\Http\Request;

/**
* Source: http://culttt.com/2013/07/29/creating-laravel-4-validation-services/
*/
abstract class Validator
{
	protected $input;
	protected $errors;
	protected $request;

	public function __construct($input = NULL, Request $request)
	{
		$this->request = $request;
		$this->input = $input ?: $this->request->all();
	}

	public function passes()
	{
		$messages =  isset(static::$messages) ? static::$messages : [];
		$validation = \Validator::make($this->input, static::$rules, $messages);

		if ($validation->passes()) {
			return true;
		}

		$this->errors = $validation->messages();
		return false;
	}

	public function getErrors()
	{
		$errors = [];
		foreach ($this->errors->all() as $erro) {
			array_push($errors, $erro);
		}
		return $errors;
	}
}
