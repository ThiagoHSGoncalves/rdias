# Teste RDias

A aplicação utiliza 2 containers:
```bash
app >> PHP 7.4 rodando em Apache 2
mysql >> Latest
```

Para subir a aplicação, acesse a raiz do projeto via terminal e execute:
```bash
make setup
```
Isso vai demorar um pouco, vá tomar um café, rs.

Feito isso, acesse: http://localhost:8081/products e verás um grid com um produto cadastrado (seed default).

O front está porco, utilizei um template antigo que tenho aqui, mas ficou uma bagunça.

Por falta de tempo acabei não utilizando o mix do Laravel para organizar os assets.

O comando 'make setup' é responsável por: 

```bash
pull das imagens do Docker
build dos containers
instalação das dependências do PHP com o Composer
ajuste de permissões da pasta /storage
create do banco de dados (e suas tabelas) utilizando Migrations
seed das tabelas com massa de dados para testes
start do queue:listen para processar os arquivos na fila de processamento
* caso queira visualizar a estrutura das tabelas, veja os arquivos dentro de '/database/migrations'
```

Para acessar a base de fora do container:
```bash
mysql -h127.0.0.1 -P3366 -urdias -p123
* essa senha nunca deveria estar aqui, mas por se tratar de um teste, fiz vista grossa, rs
```

Rotas:
```bash
GET api/product -> retorna um json com os produtos

DELETE api/product/delete -> deleta um produto
Ex.
curl -X DELETE -H 'content-type: application/json' -d '{"id":1}' http://localhost:8081/api/product/delete

POST api/product/delete
Ex.
Importe a Collection (RDias.postman_collection.json) do Postman que está na raiz do projeto.
```

Fila e Processamento:
```
O comando make setup irá iniciar o Listener que processará os arquivos.
Quando o comando: docker-compose run --no-deps app php artisan queue:listen
For exibido no terminal, o ambiente já está pronto para receber as requisições de importação dos produtos

Os jobs (aguardando processamento e com falha) podem ser consultados nas tabelas:
- failed_jobs -> mysql -h127.0.0.1 -P3366 -urdias -p123 -Drdias -e "select * from failed_jobs"
- jobs -> mysql -h127.0.0.1 -P3366 -urdias -p123 -Drdias -e "select * from jobs"
```

Testes (PHPUnit)
```
docker-compose run --no-deps --rm app ./vendor/bin/phpunit
```

