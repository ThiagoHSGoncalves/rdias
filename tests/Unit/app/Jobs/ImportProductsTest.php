<?php

namespace App\Jobs;

use Mockery as m;
use App;
use Tests\TestCase;

/**
* Testes Produtos
*/
class ImportProductsJobTest extends TestCase
{

	public function tearDown(): void
	{
		parent::tearDown();
		m::close();
	}

	/**
	* @test
	* Testa se o path informado é um diretório
	*/
	public function should_must_be_said_when_path_is_not_a_valid_directory()
	{
		$job = new ImportProducts;
		$this->assertFalse(
			$job->getFiles('')
		);
	}

	/**
	* @test
	* Testa se o arquivo informado existe
	*/
	public function should_must_be_said_when_file_not_exists()
	{
		$job = new ImportProducts;
		$this->assertFalse(
			$job->loadWorksheet('')
		);
	}

	/**
	* @test
	* Testa se o arquivo é válido (possui registros)
	*/
	public function should_must_be_said_when_file_is_valid()
	{
		$job = new ImportProducts;
		$expected = [
            'id' => 1,
            'category' => 'games',
            'name' => 'test',
            'free_shipping' => 1,
            'description' => 'test',
            'price' => 10
        ];

		$products = $job->loadWorksheet(storage_path('app/tests/products.xlsx'));
		$this->assertEquals(
		    $products[0],
			$expected
		);
	}

	/**
	* @test
	* Testa se o arquivo está vazio
	*/
	public function should_must_be_said_when_file_is_empty()
	{
		$job = new ImportProducts;
		$this->assertEquals(
			null,
			$job->loadWorksheet(storage_path('app/tests/produtos-empty.xlsx'))
		);
	}
}
