<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery as m;
use App\Product;
use Illuminate\Validation\Validator;
use Tests\TestCase;

/**
* Testes Produtos
*/
class ProductTest extends TestCase
{

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    /**
    * @test
    * Garante que é uma instancia do eloquent
    */
    public function should_be_eloquent_model_instance()
    {
        // Set
        $product = new Product;

        // Assert
        $this->assertInstanceOf(
            'Illuminate\Database\Eloquent\Model',
            $product
        );
    }
}
