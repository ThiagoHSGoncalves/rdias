<?php

namespace AcceptanceTests;

use App\Product;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
* Testes de Aceitação - Produtos
*/
class ProductFeatureTest extends TestCase
{
	/**
	* @test
	* Cadastra o produto e o vê na listagem após o redirect
	*/
	public function should_registrer_product_and_view_on_list()
	{
		// given
		Product::create([
            'name' => 'Six, six, six',
            'description' => 'the number of the beast...',
            'free_shipping' => 1,
            'price' => 666.66,
            'category' => 'games'
		]);

		// when
		$this->visit('api/product');

		// then
        $this->see('the number of the beast...');
	}

	/**
	* @test
	* Atualiza o produto e o vê na listagem após o redirect
	*/
	public function should_update_product_and_view_on_list()
	{
		// given
		$row = Product::create([
            'name' => 'Six, six, six',
            'description' => 'the number of the beast...',
            'free_shipping' => 1,
            'price' => 666.66,
            'category' => 'games'
		]);

		Product::where('id', '=', $row['id'])->update([
            'name' => 'Angra',
            'description' => 'the remains from the past to carry on',
            'free_shipping' => 0,
            'price' => 500.11,
            'category' => 'games'
		]);

		// when
		$this->visit('api/product');

		// then
		$this->see('Angra');
	}

	/**
	* @test
	* Deleta o produto e NÃO o vê na listagem após o redirect
	*/
	public function should_delete_product_and_not_view_on_list()
	{
		// given
		$row = Product::create([
            'name'          => 'Iron',
            'description'   => 'why foo?',
            'free_shipping' => 1,
            'price'         => 666.66,
            'category'      => 'games'
		]);

		Product::where('id', '=', $row['id'])->delete();

		// when
		$this->visit('api/product');

		// then
		$this->dontSee('Iron');
	}
}
