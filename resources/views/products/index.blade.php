@extends('layouts.main')

@section('content')

<!-- Sobreescrevo alguns estilos do Dante 2 -->
<div class="breadcrumb clearfix">
    <ul>
       <li><a href="/"><i class="fa fa-home"></i></a></li>
       <li><a href="">Produtos</a></li>
       <li class="activeCinza"><a href="">Listar</a></li>
    </ul>
</div>

@include('partials.alerts')

<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-striped" id="tableAdminProducts">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Categoria</th>
                    <th>Nome</th>
                    <th>Frete Grátis</th>
                    <th>Descrição</th>
                    <th>Preço (em R$)</th>
                    <th></th>
                </tr>
            </thead>
        <tbody>
        @forelse($products as $product)
            <tr>
                <td>{!! $product->id !!}</td>
                <td>{!! $product->category !!}</td>
                <td>{!! $product->name !!}</td>
                <td>{!! $product->free_shipping !!}</td>
                <td>{!! $product->description !!}</td>
                <td>{!! number_format($product->price, 2, ',', '.') !!}</td>
            </tr>
            @empty
            <tr>
                <td colspan="5">Nenhum produto encontrado</td>
            </tr>
        @endforelse
        </tbody>
        </table>
    </div>
</div>

@stop
