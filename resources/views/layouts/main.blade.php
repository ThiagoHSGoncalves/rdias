<!DOCTYPE html>

<head>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Teste Backend - RDias</title>
    <link rel="stylesheet" href="{{ asset("css/orbmm.css") }}">
    <link rel="stylesheet" href="{{ asset("css/styles.css") }}">
    <link rel="stylesheet" href="{{ asset("css/animate.css") }}">
    <link rel="stylesheet" href="{{ asset("css/all.css") }}">
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
</head>

<body>

<!--Smooth Scroll-->
<div class="smooth-overflow">
  <!--Navigation-->
  <nav class="main-header clearfix" role="navigation">
    <a class="navbar-brand" href="#">
      <span class="text-blue">RDias</span>
    </a>

    <!--Navigation Itself-->

  </nav>
  <!--/Navigation-->

  <!--MainWrapper-->
  <div class="main-wrap">
    <!--Main Menu-->
    <!--/MainMenu-->

    <div class="content-wrapper">

      <!-- Aqui é o conteúdo da View -->
      @yield('content')

    </div>
    <!-- / Content Wrapper -->
  </div>
  <!--/MainWrapper-->
</div>
<!--/Smooth Scroll-->

<!-- scroll top -->
<div class="scroll-top-wrapper hidden-xs"> <i class="fa fa-angle-up"></i> </div>
<!-- /scroll top -->

<!--Scripts-->
<script src="{{ asset("js/animation.js") }}"></script>
<script src="{{ asset("js/app.js") }}"></script>
<script src="{{ asset("js/bootstrap.js") }}"></script>
<script src="{{ asset("js/cbpHorizontalSlideOutMenu.js") }}"></script>
<script src="{{ asset("js/jquery.maskMoney.min.js") }}"></script>
<script src="{{ asset("js/modernizr.custom.js") }}"></script>
<script src="{{ asset("js/powerwidgets.js") }}"></script>
<script src="{{ asset("js/fontawesome.min.js") }}"></script>
<script src="{{ asset("js/scripts.js") }}"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<!--/Scripts-->

</body>

</html>
