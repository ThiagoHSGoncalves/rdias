{{-- Quando for sucesso --}}
@if(Session::has('success'))
<div class="alert alert-success" role="alert">
  {!! Session::get('success') !!}
</div>
@endif

{{-- Quando for sucesso --}}
@if(Session::has('message'))
<div class="alert alert-success" role="alert">
  {!! Session::get('message') !!}
</div>
@endif

{{-- Quando vier do Validator --}}
@if(Session::has('errors'))
<div class="callout callout-danger" role="alert">
	<h4>Ocorreram erros:</h4>
	<p class="bold">
		@foreach($errors as $error)
			{!! $error !!}<br />
		@endforeach
	</p>
</div>
@endif

{{-- Quando for erro isolado --}}
@if(Session::has('error'))
<div class="alert alert-danger" role="alert">
  {!! Session::get('error') !!}
</div>
@endif