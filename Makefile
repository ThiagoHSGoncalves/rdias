setup:
	# baixa as imagens docker
	docker-compose pull
	# build das imagens
	docker-compose build
	# faz uma copia do arquivo de configuracoes
	cp .env.example .env
	# sobe os containers
	docker-compose up -d
	# dispara os comandos de seed, definidos no bloco abaixo
	make seed

seed:
	# instala as dependencias do PHP
	docker-compose run --no-deps --rm app composer install
	# generate da key do Laravel
	docker-compose run --no-deps --rm app php artisan key:generate
	# ajuste das permissoes
	docker-compose run --no-deps --rm app chown -R www-data:www-data storage/
	# aguardo um pouco pro MySQL estar pronto para receber conexoes
	sleep 90
	# migrate e seed do banco de dados
	docker-compose run --no-deps --rm app php artisan migrate --seed
	# listener das filas do laravel
	docker-compose run --no-deps app php artisan queue:listen
